"""
Server Health Check Script by cipang (Version: 2020-01-19)
"""
import subprocess as sp
from typing import List, Optional

from django.conf import settings
from django.core.mail import EmailMessage


def run(units: Optional[List[str]] = None, recipients: List[Optional[str]] = None):
    if units is None:
        from serverlib import listunits
        try:
            units = listunits.find_units()
        except RuntimeError:
            units = []

    if recipients is None:
        recipients = [admin[1] for admin in settings.ADMINS]

    subject = f"{get_hostname()} server report"
    if units:
        subject = f"{subject}: {', '.join(units)}"

    message_content = "\n".join([
        exec("uptime").lstrip(),
        exec("df -h"),
        exec("journalctl --disk-usage"),
        exec("free -h"),
    ])
    message_content = f"<html><body><pre>{message_content}</pre></body></html>"
    email = EmailMessage(subject, message_content, settings.SERVER_EMAIL, recipients)
    email.content_subtype = "html"
    email.attach("last.txt", exec("last"), "text/plain")
    email.attach("auth.txt", exec("cat /var/log/auth.log | grep -v CRON | tail -n 100"), "text/plain")
    email.attach("fail2ban.txt", exec("cat /var/log/fail2ban.log | grep fail2ban.actions | tail -n 100"), "text/plain")
    for unit in units:
        email.attach(f"journalctl_{unit}.txt", exec(f"journalctl -n 100 -u \"{unit}\""), "text/plain")
    email.send()


def exec(*args) -> str:
    try:
        output = sp.check_output(args, shell=True).decode("utf-8")
        return output or " "    # Avoid zero-sized email attachments.
    except sp.CalledProcessError as e:
        return f"{e}\n"


def get_hostname() -> str:
    try:
        return exec("hostname").strip()
    except:
        return "unknown_host"
