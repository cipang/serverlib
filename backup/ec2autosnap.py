######
#
# AWS Auto Snapshot for Django - 2018-04-11
# Modified from https://github.com/viyh/aws-scripts
#
# Make snapshots for all EC2 volumes and delete snapshots older than retention time.
#
# Required IAM permissions:
#   ec2:DescribeInstances
#   ec2:DescribeVolumes
#   ec2:CreateSnapshot
#   ec2:DeleteSnapshot
#   ec2:DescribeSnapshots
#   ec2:CreateTags
#
# More: https://boto3.readthedocs.io/en/latest/guide/quickstart.html#configuration
#
import json
import logging
import subprocess as sp
from datetime import timedelta, datetime

import boto3
from django.conf import settings
from django.utils.timezone import localtime, now

from serverlib.backup import get_backup_settings


class EC2SnapshotEngine:

    def __init__(self):
        self.aws_access_key_id = getattr(settings, "AWS_ACCESS_KEY_ID", None)
        self.aws_secret_access_key = getattr(settings, "AWS_SECRET_ACCESS_KEY", None)
        self.retention_days = get_backup_settings().get("snapshot_retention_days", 7)
        self.logger = logging.getLogger(__name__)

    def create_volume_snapshot(self, instance_name, volume):
        t = localtime(now()).strftime("%Y%m%d_%H%M%S")
        v = volume.volume_id[0:10]
        description = f"autosnap_{instance_name}.{v}_{t}"
        snapshot = volume.create_snapshot(Description=description)
        if snapshot:
            snapshot.create_tags(Tags=[{"Key": "Name", "Value": description}])
            self.logger.info(f"Created snapshot {snapshot.id}.")

    def prune_volume_snapshots(self, volume):
        for s in volume.snapshots.all():
            snapshot_id, snapshot_start_time = s.id, s.start_time
            current_time = datetime.now(s.start_time.tzinfo)
            is_old_snapshot = (current_time - s.start_time) > timedelta(days=self.retention_days)
            if is_old_snapshot and s.description.startswith("autosnap_"):
                s.delete()
                self.logger.info(f"Deleted snapshot {snapshot_id} created at {snapshot_start_time}.")

    def run(self):
        instance_info = get_current_instance_info()
        self.logger.debug("Current instance info: " + repr(instance_info))
        current_instance_id = instance_info.get("instanceId")
        if not current_instance_id:
            return

        session = boto3.Session(aws_access_key_id=self.aws_access_key_id,
                                aws_secret_access_key=self.aws_secret_access_key,
                                region_name=instance_info.get("region"))
        ec2 = session.resource("ec2")
        for instance in ec2.instances.filter(Filters=[{"Name": "instance-id", "Values": [current_instance_id]}]):
            tags = {tag["Key"]: tag["Value"] for tag in instance.tags}
            instance_name = tags.get("Name", instance.id)
            volumes = ec2.volumes.filter(Filters=[{"Name": "attachment.instance-id", "Values": [instance.id]}])
            for volume in volumes:
                self.create_volume_snapshot(instance_name, volume)
                self.prune_volume_snapshots(volume)


def get_current_instance_info() -> dict:
    try:
        output = sp.check_output(["curl", "-s", "http://169.254.169.254/latest/dynamic/"
                                                "instance-identity/document"]).decode("utf-8")
        return json.loads(output)
    except:
        return {}
