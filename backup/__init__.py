"""
General Purpose Backup Engine for a Django App
Version: 2021-04-12
"""
import os
import shutil
import subprocess as sp
import tarfile
import tempfile
import time
import uuid
from pathlib import Path
from typing import Dict, Any, Optional, Iterable, Union
from urllib.parse import urlparse

import boto3
from django.conf import settings
from django.contrib.admin.views.decorators import staff_member_required
from django.db import connections
from django.http import FileResponse
from django.utils.timezone import now, localtime


class BackupEngine:
    def __init__(self):
        """
        Initialises the backup engine and its settings from the SERVER_BACKUP_SETTINGS settings variable (if any).
        """
        backup_settings = get_backup_settings()
        self.include_database: bool = backup_settings.get("include_database", True)
        self.include_files: bool = backup_settings.get("include_files", True)
        self.encrypt: bool = backup_settings.get("encrypt", False)
        self.dest_dir: str = backup_settings.get("dest_dir", settings.BASE_DIR)
        self.aws_access_key_id: str = getattr(settings, "AWS_ACCESS_KEY_ID", backup_settings.get("aws_access_key_id"))
        self.aws_secret_access_key: str = getattr(settings, "AWS_SECRET_ACCESS_KEY",
                                                  backup_settings.get("aws_secret_access_key"))
        self.aws_region: str = getattr(settings, "AWS_S3_REGION_NAME", backup_settings.get("aws_s3_region_name"))
        self.custom_file_root: Optional[Path] = None
        self.custom_file_list: Optional[Iterable[Path]] = None

        # Internal variables.
        self.backup_settings = backup_settings
        self.temp_dir: Optional[Path] = None

    def run(self) -> Union[Path, str]:
        """
        Creates a backup archive and copies the archive to the destination.
        Returns a Path object if the backup is stored in the local file system,
        or a URL string if the destination is AWS S3.
        """
        with tempfile.TemporaryDirectory() as td:
            self.temp_dir = Path(td)
            tar_filename = self.temp_dir / "tarball.tar.gz"
            tar = tarfile.open(name=tar_filename, mode="w:gz")
            try:
                if self.include_database:
                    self.backup_database(tar)
                if self.include_files:
                    self.backup_files(tar)
            finally:
                tar.close()

            if not self.encrypt:
                # If we do not encrypt, copy the file to the destination and return.
                return self.copy_to_dest(tar_filename)

            # Encrypt using gpg to a temporary file.
            passphrase = settings.SECRET_KEY
            gpg_filename = self.temp_dir / "encrypted.tar.gz"
            gpg = sp.Popen(["gpg", "--homedir", td, "--output", str(gpg_filename), "--cipher-algo", "AES256",
                            "--symmetric", "--passphrase-fd", "0", "--batch", "-q", str(tar_filename)],
                           stdin=sp.PIPE)
            gpg.stdin.write(passphrase.encode("ascii"))
            gpg.stdin.close()
            gpg.wait()
            if gpg.returncode != 0:
                raise Exception(f"gpg failed with return code {gpg.returncode}.")
            return self.copy_to_dest(gpg_filename)

    def backup_database(self, tar):
        default_conn = connections["default"]
        if default_conn.vendor != "postgresql":
            return
        db = default_conn.settings_dict
        host = str(db["HOST"])
        port = str(db["PORT"])
        username = str(db["USER"])
        password = str(db["PASSWORD"])
        db_name = str(db["NAME"])

        os.environ["PGPASSWORD"] = password
        try:
            database_backup_file = self.temp_dir / f"{db_name}.sql"
            pg_dump = sp.Popen(["pg_dump", "-h", host, "-p", port, "-U", username, "--no-owner",
                                "--no-acl", "-f", str(database_backup_file), db_name])
            pg_dump.wait()
            if pg_dump.returncode != 0:
                raise Exception(f"pg_dump failed with return code {pg_dump.returncode}.")
        finally:
            del os.environ["PGPASSWORD"]
        tar.add(database_backup_file, arcname=f"{db_name}.sql")

    def backup_files(self, tar):
        root = self.custom_file_root
        if root is None:
            root = Path(settings.MEDIA_ROOT)

        files = self.custom_file_list
        if files is None:
            files = root.rglob("*")

        for f in files:
            arc_filename = f.relative_to(root)
            tar.add(f, arcname=arc_filename, recursive=False)

    def copy_to_dest(self, source_file) -> Union[Path, str]:
        # Copy the file to the destination, or upload to S3 if a S3 URL is specified.
        backup_filename = self.generate_backup_filename()
        if isinstance(self.dest_dir, Path) or not self.dest_dir.startswith("s3://"):
            to_path = Path(self.dest_dir, backup_filename)
            shutil.copyfile(source_file, to_path)
            return to_path
        else:
            dest_url = self.dest_dir + ("/" if not self.dest_dir.endswith("/") else "") + backup_filename
            s3_url = urlparse(dest_url)
            assert s3_url.scheme == "s3"
            with open(source_file, "rb") as fp:
                s3 = boto3.resource("s3", region_name=self.aws_region,
                                    aws_access_key_id=self.aws_access_key_id,
                                    aws_secret_access_key=self.aws_secret_access_key)
                # Need to remove the root / for S3.
                s3.Bucket(s3_url.netloc).put_object(Key=s3_url.path[1:], Body=fp)
            return dest_url

    def generate_backup_filename(self) -> str:
        app_name = get_root_app_name()
        t = localtime(now()).strftime("%Y%m%d_%H%M%S")
        h = uuid.uuid4().hex[0:4]
        c = ("d" if self.include_database else "x") + ("f" if self.include_files else "x")
        e = "" if not self.encrypt else ".gpg"
        return f"{app_name}_{c}_{t}_{h}.tar.gz{e}"

    def cleanup_old_files(self, days=7):
        if self.dest_dir.startswith("s3://"):
            raise NotImplementedError("Cleanup on S3 has not implemented. Use a bucket policy to expire old backups.")
        dest_dir = Path(self.dest_dir).resolve(strict=True)
        current_time = time.time()
        for f in dest_dir.rglob(f"{get_root_app_name()}_*.tar.gz*"):
            if f.stat().st_mtime < current_time - days * 86400:
                os.remove(f)

    def __repr__(self):
        return f'<BackupEngine include_database={self.include_database} include_files={self.include_files} ' \
               f'encrypt={self.encrypt} dest_dir="{self.dest_dir}">'


def get_root_app_name() -> str:
    return Path(__file__).resolve().parent.parent.parent.name


def get_backup_settings() -> Dict[str, Any]:
    return getattr(settings, "SERVER_BACKUP_SETTINGS", {})


@staff_member_required
def backup_download_view(_) -> FileResponse:
    with tempfile.TemporaryDirectory() as td:
        b = BackupEngine()
        b.dest_dir = td
        return FileResponse(b.run().open("rb"), as_attachment=True)
