"""
serverlib.ping - A Django module to test the connection to the database
  via a HTTP request to /ping/.
"""
from random import randint

from django.db import connection
from django.http import HttpResponse
from django.views.decorators.http import require_GET


@require_GET
def ping_view(request):
    with connection.cursor() as cursor:
        cursor.execute(f"SELECT {randint(1, 99999)}")
        assert cursor.fetchone()
        return HttpResponse(b"", content_type="text/plain")
