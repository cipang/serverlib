"""
Find systemctl units of this application based on the content of the service files.
If the path of this application is in the service file, that service will be printed.
In this way we can find the services that are relevant to this application.

Author: cipang
Version: 2021-01-18
"""
from pathlib import Path
from typing import Iterable


def find_units() -> Iterable[str]:
    base_dir = str(Path(__file__).resolve().parent.parent)
    multi_target = Path("/etc/systemd/system/multi-user.target.wants")
    units = []
    for service in multi_target.glob("*.service"):
        if service.is_symlink():
            content = service.read_text()
            if base_dir in content:
                units.append(service.stem)
    if units:
        units.sort()
        return units
    else:
        raise RuntimeError("Unable to find any units.")


if __name__ == "__main__":
    print(" ".join(find_units()))
