from django.core.management.base import BaseCommand, CommandError

from serverlib.backup import BackupEngine


class Command(BaseCommand):
    help = 'Delete backup files older than certain days.'

    def add_arguments(self, parser):
        parser.add_argument("days", type=int, help="Files older than this number of days will be deleted.")

    def handle(self, *args, **options):
        try:
            engine = BackupEngine()
            engine.cleanup_old_files(options["days"])
        except Exception as ex:
            raise CommandError(ex)
