from django.core.management.base import BaseCommand, CommandError


class Command(BaseCommand):
    help = 'Email a server check report.'

    def handle(self, *args, **options):
        try:
            from serverlib.email.servercheck import run
            run()
        except Exception as ex:
            raise CommandError(ex)
