from django.apps import AppConfig


class ServerlibConfig(AppConfig):
    name = 'serverlib'
