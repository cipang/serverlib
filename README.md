# serverlib
Copyright (C) 2020-2023 Patrick Pang ([mail@patrickpang.net](mailto:mail@patrickpang.net)).

This library contains the below functions and classess that work best with Django.

- ``backup.BackupEngine``: For backing up the PostgreSQL database configured in Django.
- ``backup.ec2autosnap.EC2SnapshotEngine``: Create snapshots for the AWS EC2 instance running the app.
- ``email.SESBackend``: An email backend for sending error reports using AWS SES.
- ``email.servercheck.run()``: Send server check emails to ADMINS.
- ``utils.ping.ping_view()``: A ping view for testing the database connection.
- ``listunits.py``: A script to find the ``systemd`` units related to this app.
    - ``listunits.find_units()``: The function that can be called to find ``systemd`` units related to this app.

## Available management commands

- ``cleanupbackup``: Delete backup files older than certain days.
- ``servercheck``: Email a server check report.

## Requirements

- boto3 1.26.0+

## To Pull Changes with Git Subtree

Run this following command the pull new changes from the remote Git repo to downstream apps:

``git subtree pull --prefix serverlib https://bitbucket.org/cipang/serverlib.git master --squash``

For other commands (e.g., how to push changes back), please refer to https://gist.github.com/SKempin/b7857a6ff6bddb05717cc17a44091202