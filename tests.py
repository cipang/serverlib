import random
import string
import subprocess
import tarfile
from pathlib import Path
from tempfile import TemporaryDirectory
from unittest import SkipTest

import boto3
from django.conf import settings
from django.core.mail import EmailMessage
from django.db import connections
from django.test import SimpleTestCase, TestCase

from serverlib.backup import get_backup_settings, BackupEngine
from serverlib.email import SESBackend


class ServerLibTests(SimpleTestCase):
    @classmethod
    def setUpClass(cls):
        backup_settings = get_backup_settings()
        cls.aws_access_key_id = getattr(settings, "AWS_ACCESS_KEY_ID", backup_settings.get("aws_access_key_id"))
        cls.aws_secret_access_key = getattr(settings, "AWS_SECRET_ACCESS_KEY",
                                            backup_settings.get("aws_secret_access_key"))
        cls.aws_region = getattr(settings, "AWS_S3_REGION_NAME", backup_settings.get("aws_s3_region_name"))
        return super().setUpClass()

    def setUp(self) -> None:
        if not self.aws_access_key_id or not self.aws_secret_access_key:
            raise SkipTest("Test skipped because no AWS access key can be found.")

    def test_ses_email(self):
        msg = EmailMessage(subject="Running Unit Test: " + __name__)
        msg.body = "You receive this email because the serverlib unit test is in progress. Please ignore this email."
        msg.to = [settings.ADMINS[0][1]]
        n = SESBackend(fail_silently=False).send_messages([msg])
        self.assertEqual(n, 1)

    def test_s3_read_write(self):
        s3 = boto3.resource("s3", region_name=self.aws_region,
                            aws_access_key_id=self.aws_access_key_id,
                            aws_secret_access_key=self.aws_secret_access_key)
        filename = "".join(random.sample(string.ascii_lowercase, 8))
        content = "".join(random.sample(string.ascii_uppercase, 8))

        # Write something to S3 using put_object.
        b = s3.Bucket("unittest-storage")
        b.put_object(Key=filename, Body=content.encode())

        # Read it back.
        obj = b.Object(filename).get()
        obj_content = obj["Body"].read().decode()
        self.assertEqual(content, obj_content)

        # Delete the test object.
        b.delete_objects(Delete={"Objects": [{"Key": filename}]})

    def test_file_backup_and_encryption(self):
        be = BackupEngine()
        be.include_database = False
        be.include_files = True
        be.encrypt = True
        with TemporaryDirectory() as td:
            tmp_dir = Path(td)
            tmp_file_dir = tmp_dir / "test_files"
            tmp_file_dir.mkdir()
            tmp_files = []
            for i in range(1, 4):
                tf = tmp_file_dir / f"{i}.txt"
                tf.write_text(f"File {i}")
                tmp_files.append(tf)

            be.dest_dir = tmp_dir
            be.custom_file_root = tmp_dir
            be.custom_file_list = tmp_files
            encrypted_backup = be.run()

            # Decrypt the backup and check the content.
            gpg = subprocess.Popen(["gpg", "--batch", "-q", "--passphrase-fd", "0", "-d", str(encrypted_backup)],
                                   stdin=subprocess.PIPE, stdout=subprocess.PIPE)
            gpg.stdin.write(settings.SECRET_KEY.encode("ascii"))
            gpg.stdin.close()
            tar = tarfile.open(fileobj=gpg.stdout, mode="r|gz")
            try:
                names = set()
                content = set()
                for tarinfo in tar:
                    names.add(tarinfo.name)
                    with tar.extractfile(tarinfo) as f:
                        content.add(f.read().decode())
                self.assertEqual(names, {"test_files/1.txt", "test_files/2.txt", "test_files/3.txt"})
                self.assertEqual(content, {"File 1", "File 2", "File 3"})
            finally:
                gpg.stdout.close()
                tar.close()
            gpg.wait()
            self.assertEqual(gpg.returncode, 0)


class ServerLibDatabaseTests(TestCase):
    def setUp(self) -> None:
        if connections["default"].vendor != "postgresql":
            raise SkipTest("Not using PostgreSQL.")

    def test_local_database_backup(self):
        be = BackupEngine()
        be.include_database = True
        be.include_files = False
        be.encrypt = False
        with TemporaryDirectory() as td:
            be.dest_dir = td
            output_filename = be.run()

            tar = tarfile.open(output_filename, "r:gz")
            try:
                sql_filename = tar.getnames()[0]
                self.assertTrue(sql_filename.endswith(".sql"))
                sql_file = tar.extractfile(sql_filename)
                result = False
                for bline in sql_file:
                    if "CREATE TABLE " in bline.decode():
                        result = True
                        break
                self.assertTrue(result)
            finally:
                tar.close()
